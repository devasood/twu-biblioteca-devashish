package com.twu.biblioteca.io;

import java.io.InputStream;
import java.util.Scanner;

public class ConsoleReader implements Reader{
  private Scanner scan;

  public ConsoleReader(InputStream inputStream){
    scan=new Scanner(inputStream);
  }

  public int readInteger() throws NumberFormatException {
    int option = Integer.parseInt(scan.nextLine());

    return option;
  }

  public String readString() {
    return scan.nextLine();
  }
}
