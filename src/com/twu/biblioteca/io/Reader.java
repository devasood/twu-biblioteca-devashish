package com.twu.biblioteca.io;

public interface Reader {
  int readInteger();
  String readString();
}
