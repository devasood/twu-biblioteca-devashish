package com.twu.biblioteca.io;

import com.twu.biblioteca.core.Book;
import com.twu.biblioteca.core.LibraryManager;
import com.twu.biblioteca.core.Movie;

import java.util.List;
import java.util.Properties;

import static com.twu.biblioteca.parser.PropertyParser.*;

public class ConsoleWriter {
  private final Properties properties;
  private final String BOOK_FORMAT;
  private final String MOVIE_FORMAT;

  public ConsoleWriter(Properties properties) {
    this.properties = properties;
    BOOK_FORMAT = "| %-5s | %-50s | %-20s | %-14s |%n";
    MOVIE_FORMAT = "| %-5s | %-50s | %-20s | %-14s | %-10s %n";
  }

  public void welcomeMessage() {
    System.out.println(properties.getProperty(WelcomeMessage));
  }

  public void menu(LibraryManager libraryManager) {
    demarcator();
    System.out.println("MAIN MENU");
    demarcator();
    System.out.println("Option\t| Action");
    demarcator();
    System.out.println(libraryManager);
    System.out.println("Please select an option:");
  }

  private void display(Book book) {
    String bookAttributes[] = book.toString().split(",");
    System.out.printf(BOOK_FORMAT, bookAttributes[0], bookAttributes[1],
        bookAttributes[2], bookAttributes[3]);
  }

  private void display(Movie movie) {
    String movieAttributes[] = movie.toString().split(",");
    System.out.printf(MOVIE_FORMAT, movieAttributes[0], movieAttributes[1],
        movieAttributes[2], movieAttributes[3], movieAttributes[4]);
  }

  public void invalidMenuOption() {
    System.out.println(properties.getProperty(InvalidMenuOption));
  }

  public void unsuccessfulCheckoutMessage() {
    System.out.println(properties.getProperty(UnsuccessfulCheckoutMessage) + "\n");
  }

  public void successfulCheckoutMessage() {
    System.out.println(properties.getProperty(SuccessfulCheckoutMessage) + "\n");
  }

  public void successfulReturnMessage() {
    System.out.println(properties.getProperty(SuccessfulReturnMessage) + "\n");
  }

  public void unsuccessfulReturnMessage() {
    System.out.println(properties.getProperty(UnsuccessfulReturnMessage) + "\n");
  }

  public void askForISBN() {
    System.out.println(properties.getProperty(EnterISBN));
  }

  public void askForTitle() {
    System.out.println(properties.getProperty(EnterTitle));
  }

  public void askForAuthor() {
    System.out.println(properties.getProperty(EnterAuthor));
  }

  public void askForYearPublished() {
    System.out.println(properties.getProperty(EnterYearPublished));
  }

  public void demarcator() {
    for (int i = 0; i < 230; i++) {
      System.out.print("-");
    }
    System.out.println();
  }

  public void bookList(List<Book> books) {
    demarcator();
    System.out.println("LIST BOOKS");
    demarcator();

    System.out.printf(BOOK_FORMAT, "ISBN", "TITLE", "AUTHOR", "YEAR PUBLISHED");
    demarcator();

    for (Book book : books) {
      display(book);
    }
    System.out.println();
  }

  public void movieList(List<Movie> movies) {
    demarcator();
    System.out.println("LIST MOVIES");
    demarcator();

    System.out.printf(MOVIE_FORMAT,
        "ISBN", "NAME", "YEAR", "DIRECTOR", "RATINGS");

    demarcator();

    for (Movie movie : movies) {
      display(movie);
    }
    System.out.println();
  }
}
