package com.twu.biblioteca.parser;

import java.io.*;
import java.util.Properties;

public class PropertyParser {
  public static final String WelcomeMessage = "WelcomeMessage";
  public static final String UnsuccessfulCheckoutMessage = "UnsuccessfulCheckoutMessage";
  public static final String SuccessfulCheckoutMessage = "SuccessfulCheckoutMessage";
  public static final String SuccessfulReturnMessage = "SuccessfulReturnMessage";
  public static final String UnsuccessfulReturnMessage = "UnsuccessfulReturnMessage";
  public static final String InvalidMenuOption = "InvalidMenuOption";
  public static final String EnterISBN = "EnterISBN";
  public static final String EnterTitle = "EnterTitle";
  public static final String EnterAuthor = "EnterAuthor";
  public static final String EnterYearPublished = "EnterYearPublished";

  private final String propertyFileName = "properties";

  public Properties getProperties() {
    Properties properties = new Properties();
    InputStream input;

    try {
      input = new FileInputStream(propertyFileName);

      properties.load(input);

      input.close();
    } catch (IOException ex) {
      ex.printStackTrace();
    }

    return properties;
  }


}
