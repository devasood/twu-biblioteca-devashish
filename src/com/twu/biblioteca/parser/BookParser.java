package com.twu.biblioteca.parser;

import com.twu.biblioteca.core.Book;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BookParser {
  public List<Book> readBookFile() {
    List<Book> books = new ArrayList();
    try {
      Scanner bookReader = new Scanner(new File("books"));
      while (bookReader.hasNext()) {
        String line[] = bookReader.nextLine().split(",");
        books.add(new Book(Integer.parseInt(line[0]), line[1],
            line[2], Integer.parseInt(line[3])));
      }
      bookReader.close();
    } catch (FileNotFoundException e) {
    }
    return books;
  }
}
