package com.twu.biblioteca.parser;

import com.twu.biblioteca.core.Movie;
import com.twu.biblioteca.core.Rating;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MovieParser {
  public List<Movie> readMovieFile() {
    List<Movie> movies = new ArrayList();
    try {
      Scanner bookReader = new Scanner(new File("movies"));
      while (bookReader.hasNext()) {
        String line[] = bookReader.nextLine().split(",");
        movies.add(new Movie(Integer.parseInt(line[0]), line[1],
            Integer.parseInt(line[2]),line[3], Rating.valueOf(line[4])));
      }
      bookReader.close();
    } catch (FileNotFoundException e) {
    }
    return movies;
  }
}
