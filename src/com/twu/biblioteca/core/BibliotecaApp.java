package com.twu.biblioteca.core;

/*
* The driver for the Library class.
* Customers interact with the BibliotecaApp to access the digital library.
* */
public class BibliotecaApp {

  public static void main(String[] args) {
    Biblioteca app = new Biblioteca(System.in);
    app.start();
  }
}
