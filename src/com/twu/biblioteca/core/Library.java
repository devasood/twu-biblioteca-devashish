package com.twu.biblioteca.core;
/*
* Imitates a digital availableBooks.
* */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.twu.biblioteca.core.LibraryItemType.*;

public class Library {
  private List<Book> availableBooks = new ArrayList();
  private List<Book> checkedOutBooks = new ArrayList();

  private List<Movie> availableMovies = new ArrayList();
  private List<Movie> checkedOutMovies = new ArrayList();

  public void add(LibraryItemType libraryItemType, List items) {
    if (BOOK == libraryItemType)
      addBooks(items);
    if (MOVIE == libraryItemType)
      addMovies(items);
  }

  public List list(LibraryItemType libraryItemType) {
    if (BOOK == libraryItemType)
      return listBooks();
    if (MOVIE == libraryItemType)
      return listMovie();
    return availableMovies;
  }

  private List<Book> listBooks() {
    return Collections.unmodifiableList(availableBooks);
  }

  private List<Movie> listMovie() {
    return Collections.unmodifiableList(availableMovies);
  }

  public Book checkoutBook(int id) throws InvalidBookException {
    for (Book book : availableBooks) {
      if (book.hasId(id)) {
        checkedOutBooks.add(book);
        availableBooks.remove(book);
        return book;
      }
    }
    throw new InvalidBookException();
  }

  public void returnBook(Book book) throws InvalidBookException {
    if (checkedOutBooks.contains(book)) {
      checkedOutBooks.remove(book);
      availableBooks.add(book);
      return;
    }
    throw new InvalidBookException();
  }

  private void addBooks(List<Book> books) {
    availableBooks.addAll(books);
  }

  private void addMovies(List<Movie> movies) {
    availableMovies.addAll(movies);
  }
}
