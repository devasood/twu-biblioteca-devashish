package com.twu.biblioteca.core;

import com.twu.biblioteca.io.ConsoleReader;
import com.twu.biblioteca.io.ConsoleWriter;
import com.twu.biblioteca.io.Reader;

import static com.twu.biblioteca.core.LibraryItemType.*;

public interface LibraryActionType {
  void execute(Library library, Reader reader, ConsoleWriter consoleWriter);

  LibraryActionType LIST_BOOKS =
      (library, reader, consoleWriter) -> consoleWriter.bookList(library.list(BOOK));

  LibraryActionType LIST_MOVIES =
      (library, reader, consoleWriter) -> consoleWriter.movieList(library.list(MOVIE));

  LibraryActionType CHECKOUT_BOOK = (library, reader, consoleWriter) -> {
    consoleWriter.askForISBN();
    int id = reader.readInteger();
    try {
      library.checkoutBook(id);
      consoleWriter.successfulCheckoutMessage();

    } catch (InvalidBookException e) {
      consoleWriter.unsuccessfulCheckoutMessage();
    }
  };

  LibraryActionType RETURN_BOOK = (library, reader, consoleWriter) -> {

    consoleWriter.askForISBN();
    int id = reader.readInteger();

    consoleWriter.askForTitle();
    String title = reader.readString();

    consoleWriter.askForAuthor();
    String author = reader.readString();

    consoleWriter.askForYearPublished();
    int yearPublished = reader.readInteger();

    try {
      library.returnBook(new Book(id, title, author, yearPublished));
      consoleWriter.successfulReturnMessage();

    } catch (InvalidBookException e) {
      consoleWriter.unsuccessfulReturnMessage();
    }
    //should I use bookParser and make Book constructor package protected
  };

  LibraryActionType CHECKOUT_MOVIE=CHECKOUT_BOOK;

  LibraryActionType RETURN_MOVIE=RETURN_BOOK;

  LibraryActionType QUIT = (library, consoleReader, consoleWriter) -> System.exit(0);

}
