package com.twu.biblioteca.core;

public class Movie {
  private final int id;
  private final String name;
  private final int year;
  private final String director;
  private final Rating rating;

  public Movie(int id, String name, int year, String director, Rating rating) {
    this.id = id;
    this.name = name;
    this.year = year;
    this.director = director;
    this.rating = rating;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Movie movie = (Movie) o;

    if (id != movie.id) return false;
    if (year != movie.year) return false;
    if (name != null ? !name.equals(movie.name) : movie.name != null) return false;
    if (director != null ? !director.equals(movie.director) : movie.director != null) return false;
    return rating == movie.rating;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + year;
    result = 31 * result + (director != null ? director.hashCode() : 0);
    result = 31 * result + (rating != null ? rating.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    String COMMA = ",";
    return id + COMMA + name + COMMA + year + COMMA + director +
        COMMA + rating.toString();
  }
}
