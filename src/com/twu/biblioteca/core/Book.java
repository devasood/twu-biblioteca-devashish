package com.twu.biblioteca.core;

/*
* Imitates a book in real life. Immutable.
* */
public class Book {

  private final int id;
  private final String title;
  private final String author;
  private final int yearPublished;

  public Book(int id, String title, String author, int yearPublished) {
    this.id = id;
    this.title = title;
    this.author = author;
    this.yearPublished = yearPublished;
  }

  public boolean hasId(int id) {
    return this.id == id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Book book = (Book) o;

    if (id != book.id) return false;
    if (yearPublished != book.yearPublished) return false;
    if (title != null ? !title.equals(book.title) : book.title != null) return false;
    return author != null ? author.equals(book.author) : book.author == null;

  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (title != null ? title.hashCode() : 0);
    result = 31 * result + (author != null ? author.hashCode() : 0);
    result = 31 * result + yearPublished;
    return result;
  }

  @Override
  public String toString() {
    String COMMA = ",";
    return id + COMMA + title + COMMA + author +
        COMMA + yearPublished;
  }
}
