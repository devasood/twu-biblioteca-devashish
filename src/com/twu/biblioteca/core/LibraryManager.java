package com.twu.biblioteca.core;

import com.twu.biblioteca.io.ConsoleWriter;
import com.twu.biblioteca.io.Reader;

import java.util.ArrayList;
import java.util.List;

public class LibraryManager {
  private List<String> menu = new ArrayList();
  private List<LibraryActionType> actions = new ArrayList();

  public void add(String s, LibraryActionType actionType) {
    menu.add(s);
    actions.add(actionType);
  }

  public void executeUserCommand(Library library, Reader reader, ConsoleWriter consoleWriter)
      throws NumberFormatException {
    int action = reader.readInteger();

    for (int i = 0; i < menu.size(); i++) {
      if (action == (i + 1)) {
        actions.get(i).execute(library, reader, consoleWriter);
        consoleWriter.menu(this);
        return;
      }
    }

    consoleWriter.invalidMenuOption();
  }

  @Override
  public String toString() {
    String menu = "";

    for (int i = 0; i < this.menu.size(); i++) {
      menu += (i + 1) + ".\t\t| " + this.menu.get(i) + "\n";
    }

    return menu;
  }
}
