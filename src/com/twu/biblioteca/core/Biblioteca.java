package com.twu.biblioteca.core;

import com.twu.biblioteca.io.ConsoleReader;
import com.twu.biblioteca.io.ConsoleWriter;
import com.twu.biblioteca.io.Reader;
import com.twu.biblioteca.parser.BookParser;
import com.twu.biblioteca.parser.MovieParser;
import com.twu.biblioteca.parser.PropertyParser;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import static com.twu.biblioteca.core.LibraryActionType.*;
import static com.twu.biblioteca.core.LibraryActionType.RETURN_BOOK;
import static com.twu.biblioteca.core.LibraryItemType.BOOK;
import static com.twu.biblioteca.core.LibraryItemType.MOVIE;
/*
* The controller class for the application.
* Handles dependency injection.
* */
public class Biblioteca {
  private final ConsoleWriter consoleWriter;
  private final Reader reader;

  private final Library library;
  private final LibraryManager libraryManager;

  Biblioteca(InputStream inputStream) {
    Properties properties = new PropertyParser().getProperties();

    reader = new ConsoleReader(inputStream);
    consoleWriter = new ConsoleWriter(properties);

    library = new Library();
    initializeLibrary(library);

    libraryManager = new LibraryManager();
    initializeLibraryManager();
  }

  public void start() {
    displayWelcomeMessage();

    displayMenu();

    while (true) {
      executeUserCommand();
    }
  }

  private void displayWelcomeMessage() {
    consoleWriter.welcomeMessage();
  }

  private void displayMenu() {
    consoleWriter.menu(libraryManager);
  }

  private void executeUserCommand() {
    try {
      libraryManager.executeUserCommand(library, reader, consoleWriter);

    } catch (NumberFormatException e) {
      consoleWriter.invalidMenuOption();
    }
  }

  private void initializeLibrary(Library library) {
    List<Book> books = new BookParser().readBookFile();
    library.add(BOOK, books);

    List<Movie> movies = new MovieParser().readMovieFile();
    library.add(MOVIE, movies);
  }

  private void initializeLibraryManager() {
    libraryManager.add("Quit", QUIT);
    libraryManager.add("List Books", LIST_BOOKS);
    libraryManager.add("List Movies", LIST_MOVIES);
    libraryManager.add("Checkout Book", CHECKOUT_BOOK);
    libraryManager.add("Return Book", RETURN_BOOK);
  }
}
