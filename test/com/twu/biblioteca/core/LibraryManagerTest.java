package com.twu.biblioteca.core;

import com.twu.biblioteca.io.ConsoleWriter;
import com.twu.biblioteca.io.Reader;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.twu.biblioteca.core.LibraryActionType.*;
import static com.twu.biblioteca.core.LibraryItemType.BOOK;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class LibraryManagerTest {
  private Book book1 = new Book(1, "ABC", "DEF", 2000);

  @Test
  public void itShouldBePossibleToAddThingsToMenu() throws Exception {
    LibraryManager libraryManager = new LibraryManager();
    libraryManager.add("Item 1", LIST_BOOKS);
    libraryManager.add("Item 2", LIST_BOOKS);
  }

  @Test
  public void menuShouldDisplayValuesAdded() throws Exception {
    LibraryManager libraryManager = new LibraryManager();
    libraryManager.add("Item 1", LIST_BOOKS);
    libraryManager.add("Item 2", LIST_BOOKS);

    assertEquals("Item 1 test", true, libraryManager.toString().contains("Item 1"));
    assertEquals("Item 2 test", true, libraryManager.toString().contains("Item 2"));

    assertEquals("Fake item test", false, libraryManager.toString().contains("Item 3"));
  }

  @Test
  public void menuShouldDisplayValuesInOrderOfAddition() throws Exception {
    LibraryManager libraryManager = new LibraryManager();
    libraryManager.add("Item 1", LIST_BOOKS);
    libraryManager.add("Item 2", LIST_BOOKS);

    Pattern pattern1 = Pattern.compile("(.*)1(.*)Item 1(.*)");
    Pattern pattern2 = Pattern.compile("(.*)2(.*)Item 2(.*)");

    Pattern pattern3 = Pattern.compile("(.*)1(.*)Item 2(.*)");

    Matcher matcher1 = pattern1.matcher(libraryManager.toString());
    Matcher matcher2 = pattern2.matcher(libraryManager.toString());

    Matcher matcher3 = pattern3.matcher(libraryManager.toString());

    assertEquals("Item 1 in order", true, matcher1.find());
    assertEquals("Item 2 in order", true, matcher2.find());
    //sad part where expected to fail/return false
    assertEquals("Item 2 out of order", false, matcher3.find());
  }

  @Test
  public void libraryManagerShouldDisplaySuccessCheckoutMessage() {
    LibraryManager libraryManager = new LibraryManager();
    libraryManager.add("Checkout", CHECKOUT_BOOK);

    Library library = new Library();
    library.add(BOOK, asList(book1));

    Reader reader = mock(Reader.class);
    ConsoleWriter consoleWriter = mock(ConsoleWriter.class);

    when(reader.readInteger()).thenReturn(1);

    libraryManager.executeUserCommand(library, reader, consoleWriter);

    verify(consoleWriter).successfulCheckoutMessage();
  }

  @Test
  public void libraryManagerShouldDisplayFailCheckoutMessage() throws InvalidBookException {
    LibraryManager libraryManager = new LibraryManager();
    libraryManager.add("Checkout", CHECKOUT_BOOK);

    Library library = new Library();

    Reader reader = mock(Reader.class);
    ConsoleWriter consoleWriter = mock(ConsoleWriter.class);

    when(reader.readInteger()).thenReturn(1);

    libraryManager.executeUserCommand(library, reader, consoleWriter);

    verify(consoleWriter).unsuccessfulCheckoutMessage();
  }

  @Test
  public void libraryManagerShouldDisplayUnsuccessfulReturnMessage() throws InvalidBookException {
    LibraryManager libraryManager = new LibraryManager();
    libraryManager.add("Checkout", CHECKOUT_BOOK);
    libraryManager.add("Return Book", RETURN_BOOK);

    Library library = new Library();
    library.add(BOOK, asList(book1));

    Reader reader = mock(Reader.class);
    ConsoleWriter consoleWriter = mock(ConsoleWriter.class);

    when(reader.readInteger()).thenReturn(1);

    libraryManager.executeUserCommand(library, reader, consoleWriter);

    when(reader.readInteger()).thenReturn(2);
    when(reader.readString()).thenReturn("Wrong");

    libraryManager.executeUserCommand(library, reader, consoleWriter);

    verify(consoleWriter).unsuccessfulReturnMessage();
  }


}