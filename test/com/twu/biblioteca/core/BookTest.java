package com.twu.biblioteca.core;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BookTest {
  private Book book;
  private int id = 1;
  private String bookName = "Harry Potter";
  private String author = "JK Rowling";
  private int yearPublished = 1990;

  @Before
  public void setUp() throws Exception {
    book = new Book(id, bookName, author, yearPublished);
  }


  @Test
  public void bookShouldDisplayDetailsAsString() {
    //Did not string equals as it uses implementation details.
    assertEquals("id test", true,
        book.toString().contains("" + id));

    assertEquals("title test", true,
        book.toString()
            .contains(bookName.toString()));

    assertEquals("author test", true,
        book.toString()
            .contains(author.toString()));

    assertEquals("year published test", true,
        book.toString()
            .contains("" + yearPublished));
  }


}