package com.twu.biblioteca.core;

import org.junit.Test;

import static com.twu.biblioteca.core.LibraryItemType.BOOK;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class LibraryTest {
  private Book book1 = new Book(2, "Fellowship of the Ring", "JRR TOLKIEN", 1954);
  private Book book2 = new Book(1, "The Hobbit", "JRR TOLKIEN", 1954);

  @Test
  public void listBooksTest() {
    Library library = new Library();
    library.add(BOOK, asList(book2, book1));
    assertEquals("Random book1 test", true,
        library.list(BOOK)
            .contains(book1));
  }

  @Test
  public void checkoutBookShouldReturnBookSuccessfully() throws InvalidBookException {
    Library library = new Library();
    library.add(BOOK, asList(book1));

    assertEquals(book1, library.checkoutBook(2));
  }

  @Test(expected = InvalidBookException.class)
  public void checkoutBookShouldFail() throws InvalidBookException {
    Library library = new Library();

    assertNotEquals(book1, library.checkoutBook(2));
  }

  @Test(expected = InvalidBookException.class)
  public void returnBookThrowsExceptionOnWrongBook() throws InvalidBookException {
    Library library = new Library();
    library.add(BOOK, asList(book1));

    Book book = library.checkoutBook(2);
    library.returnBook(new Book(11, "Wrong Name", "Wrong Auth", 2000));
  }

  @Test
  public void returnBookAcceptsCorrectBookSilently() throws InvalidBookException {
    Library library = new Library();
    library.add(BOOK, asList(book1));

    Book book = library.checkoutBook(2);
    library.returnBook(book);
  }
}